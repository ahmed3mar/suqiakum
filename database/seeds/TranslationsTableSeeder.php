<?php

use Illuminate\Database\Seeder;

class TranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('translator_languages')->insert([[
            'locale' => 'en',
            'name' => 'English',
        ], [
            'locale' => 'ar',
            'name' => 'Arabic',
        ]]);

        $translations = [
            'header.home'       => 'HOME',
            'header.arabic'      => 'عربي',
        ];
        foreach ($translations as $key => $translation) {
            $ex = explode('.', $key);
            DB::table('translator_translations')->insert([
                'locale' => 'en',
                'namespace' => '*',
                'group' => $ex[0],
                'item' => $ex[1],
                'text' => $translation,
            ]);
        }
    }
}

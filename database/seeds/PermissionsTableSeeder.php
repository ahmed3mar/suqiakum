<?php

use App\User;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permissions = [
            'users'                 => ['add-user'  , 'edit-user'   , 'delete-user'],
            'cities'                => ['add-city'  , 'edit-city'   , 'delete-city'],
            'neighborhoods'         => ['add-neighborhood'  , 'edit-neighborhood'   , 'delete-neighborhood'],
            'translations'          => ['add-translation'  , 'edit-translation'   , 'delete-translation'],
            'mosques'               => ['add-mosque'  , 'edit-mosque'   , 'delete-mosque'],
            'companies'               => ['add-company'  , 'edit-company'   , 'delete-company'],
            'products'               => ['add-product'  , 'edit-product'   , 'delete-product'],
            'options'               => ['edit-options'],
            'contacts'               => ['view-contacts', 'edit-contact', 'delete-contact'],
            'orders'               => ['view-order', 'delete-order'],
        ];

        $per = [];
        foreach ($permissions as $category => $dd) {
            foreach ($dd as $p) {
                $permission = new \App\Permission();
                $permission->category = $category;
                $permission->name = $p;
                $permission->save();
                $per[] = $permission->id;
            }
        }

        $user = User::all();
        $user[0]->permissions()->attach($per);

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    function company() {
        return $this->belongsTo(Company::class);
    }

    function getOrderUrlAttribute() {
        return route('order.product', $this->id);
    }
}

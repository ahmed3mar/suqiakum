<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Neighborhood extends Model
{
    function city() {
        return $this->belongsTo(City::class);
    }

    function companies() {
        return $this->belongsToMany(Company::class, 'company_places');
    }

    function mosques() {
        return $this->hasMany(Mosque::class);
    }

}

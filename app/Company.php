<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    function user() {
        return $this->belongsTo(User::class);
    }
    function places() {
        return $this->hasMany(CompanyPlace::class);
    }
    function products() {
        return $this->hasMany(Product::class);
    }

    function getImageUrlAttribute() {
        return route('image', ['company', $this->image]);
    }

    function getUrlAttribute() {
        return route('company', $this->id);
    }

    function getOrderUrlAttribute() {
        return route('order.company', $this->id);
    }
}

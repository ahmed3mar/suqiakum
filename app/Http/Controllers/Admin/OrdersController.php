<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class OrdersController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['view-order', 'delete-order'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'orders');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ordersData = $this->data()->getData();
        $orders = $ordersData->data;
        $recordsTotal = $ordersData->recordsTotal;
        return view('admin.orders', compact('orders', 'recordsTotal'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $isOrder = \request()->get('order');

        if ($isOrder) $query = Order::query();
        else $query = Order::query()->orderBy('id','desc')->limit(10);

        return Datatables::of($query)
            ->editColumn('name', function (Order $order) {
                return $order->name . '<br />' . $order->phone;
            })
            ->editColumn('products', function (Order $order) {
                $products = @json_decode($order->products);
                $_html = "";
                if($products && is_array($products))
                {
                    foreach ($products as $product) {
                        $_product = Product::find($product->product_id);
                        if(!$_product) continue;
                        $_html .= $_product->name . ' - ' . ($_product->company ? $_product->company->name : 'company deleted!') . '<br />';
                    }
                }
                return $_html;
            })
            ->editColumn('price', function (Order $order) {
                $products = @json_decode($order->products);
                $total_price = 0;
                if($products && is_array($products))
                foreach ($products as $product) {
                    $_product = Product::find($product->product_id);
                    if(!$_product) continue;
                    $total_price += $product->quantity * $_product->price;
                }
                return $total_price;
//                return $order->quantity . ' * ' . $order->product->price . '<br>' . ($order->quantity * $order->product->price);
            })
            ->editColumn('mosque', function (Order $order) {
                return $order->mosque->name;
            })
            ->addColumn('options', function (Order $order) {

                $back = "";

                if (can(['view-order', 'delete-order'])) {
                    if ( can('view-order') ) $back .= data_edit_btn($order);
                    if ( can('delete-order') ) $back .= data_delete_btn($order);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options', 'products', 'price', 'name'])
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('add-order')) return no_permission();

        $this->validate($request, [
            'name'          => 'required|max:255',
        ]);

        $order = new Order();
        $order->name = $request->name;
        $order->order_order = $request->order_order;

        $order->save();

        return $order;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $order = Order::findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $order
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-order')) return no_permission();

        $this->validate($request, [
            'name'          => 'required|max:255',
        ]);

        $order = Order::find($id);
        $order->name = $request->name;
        $order->order_order = $request->order_order;
        $order->save();

        return $order;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (!auth()->user()->can('delete-order')) return no_permission();

        try{
            $order = Order::findOrFail($id);
            $order->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }
}

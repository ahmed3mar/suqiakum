<?php

namespace App\Http\Controllers\Admin;

use App\Neighborhood;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class NeighborhoodsController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['add-neighborhood', 'edit-neighborhood', 'delete-neighborhood'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'neighborhoods');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $neighborhoodsData = $this->data()->getData();
        $neighborhoods = $neighborhoodsData->data;
        $recordsTotal = $neighborhoodsData->recordsTotal;
        return view('admin.neighborhoods', compact('neighborhoods', 'recordsTotal'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $isOrder = \request()->get('order');

        if ($isOrder) $query = Neighborhood::query();
        else $query = Neighborhood::query()->orderBy('id','desc')->limit(10);

        return Datatables::of($query)
            ->addColumn('city', function (Neighborhood $neighborhood) {
                return $neighborhood->city->name;
            })
            ->addColumn('options', function (Neighborhood $neighborhood) {

                $back = "";

                if (can(['edit-neighborhood', 'delete-neighborhood'])) {
                    if ( can('edit-neighborhood') ) $back .= data_edit_btn($neighborhood);
                    if ( can('delete-neighborhood') ) $back .= data_delete_btn($neighborhood);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options', 'image_url', 'name'])
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('add-neighborhood')) return no_permission();

        $this->validate($request, [
            'name'          => 'required|max:255',
            'city_id'       => 'required',
        ]);

        $neighborhood = new Neighborhood();
        $neighborhood->name = $request->name;
        $neighborhood->city_id = $request->city_id;
        $neighborhood->neighborhood_order = $request->neighborhood_order;

        $neighborhood->save();

        return $neighborhood;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $neighborhood = Neighborhood::findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $neighborhood
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    function list($city_id) {
        return Neighborhood::whereCityId($city_id)->pluck('name', 'id');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-neighborhood')) return no_permission();

        $this->validate($request, [
            'name'          => 'required|max:255',
            'city_id'       => 'required',
        ]);

        $neighborhood = Neighborhood::find($id);
        $neighborhood->name = $request->name;
        $neighborhood->city_id = $request->city_id;
        $neighborhood->neighborhood_order = $request->neighborhood_order;
        $neighborhood->save();

        return $neighborhood;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (!auth()->user()->can('delete-neighborhood')) return no_permission();

        try{
            $neighborhood = Neighborhood::findOrFail($id);
            $neighborhood->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }
}

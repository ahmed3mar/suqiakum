<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ProductsController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['add-product', 'edit-product', 'delete-product'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'products');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productsData = $this->data()->getData();
        $products = $productsData->data;
        $recordsTotal = $productsData->recordsTotal;
        return view('admin.products', compact('products', 'recordsTotal'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $isOrder = \request()->get('order');

        if ($isOrder) $query = Product::query();
        else $query = Product::query()->orderBy('id','desc')->limit(10);

        return Datatables::of($query)
            ->editColumn('image', function (Product $product) {
                return img(url('image/product/' . $product->image), "", "width='75px'");
            })
            ->addColumn('company', function (Product $product) {
                return $product->company->name;
            })
            ->addColumn('options', function (Product $product) {

                $back = "";

                if (can(['edit-product', 'delete-product'])) {
                    if ( can('edit-product') ) $back .= data_edit_btn($product);
                    if ( can('delete-product') ) $back .= data_delete_btn($product);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options', 'image', 'name'])
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('add-product')) return no_permission();

        $this->validate($request, [
            'name'          => 'required|max:255',
            'description'       => 'required',
            'file'       => 'required',
            'price'       => 'required',
            'company_id'       => 'required',
        ]);

        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->company_id = $request->company_id;

        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $path = $request->file->storePublicly('products');
                if( $path ) {
                    $product->image = str_replace('products/', '', $path);
                }
            }
        }

        $product->save();

        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $product = Product::findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $product
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-product')) return no_permission();

        $this->validate($request, [
            'name'          => 'required|max:255',
            'description'       => 'required',
            'price'       => 'required',
            'company_id'       => 'required',
        ]);

        $product = Product::find($id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->company_id = $request->company_id;

        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $path = $request->file->storePublicly('products');
                if( $path ) {
                    $product->image = str_replace('products/', '', $path);
                }
            }
        }

        $product->save();

        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (!auth()->user()->can('delete-product')) return no_permission();

        try{
            $product = Product::findOrFail($id);
            $product->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }
}

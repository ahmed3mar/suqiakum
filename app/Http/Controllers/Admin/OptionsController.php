<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Option;
use Illuminate\Http\Request;

class OptionsController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can('edit-options')) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'options');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        parseOptions();
        option('site_title');
        return view('admin.options', compact('options'));
    }

    public function update(Request $request) {

        $option_type = $request->get('type');

        if ($option_type == 'global') {
            $this->validate($request, [
                'site_title' => 'required|max:255',
                'home_title' => 'required|max:255',
                'site_description' => 'required',
                'site_keywords' => 'required',
            ]);

            $data = [
                'site_title', 'site_title_ar',
                'home_title', 'home_title_ar',
                'site_description', 'site_description_ar',
                'site_keywords', 'site_keywords_ar',
            ];

            foreach ($data as $item) {
                Option::where(['key' => $item])->update(['value' => $request->get($item)]);
            }
        }

        if ($option_type == 'brand') {
            $this->validate($request, [
                'cars_title'        => 'required|max:70',
                'cars_description'  => 'required',

                'cars_title_ar'        => 'required|max:70',
                'cars_description_ar'  => 'required',
            ]);

            $data = [
                'cars_title', 'cars_title_ar',
                'cars_description', 'cars_description_ar',
            ];

            foreach ($data as $item) {
                Option::where(['key' => $item])->update(['value' => $request->get($item)]);
            }
        }

        if ($option_type == 'years') {
            $this->validate($request, [
                'from_year' => 'required|integer:4',
                'to_year' => 'required|to_year',
            ]);

            Option::where(['key' => 'from_year'])->update(['value' => $request->get('from_year')]);
            Option::where(['key' => 'to_year'])->update(['value' => $request->get('to_year')]);
        }

        if ($option_type == 'close') {
            Option::where(['key' => 'site_close'])->update(['value' => $request->get('site_close')]);
            Option::where(['key' => 'site_close_message'])->update(['value' => $request->get('site_close_message')]);
            Option::where(['key' => 'site_close_message_ar'])->update(['value' => $request->get('site_close_message_ar')]);
        }

    }


}

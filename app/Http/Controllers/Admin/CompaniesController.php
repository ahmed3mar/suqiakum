<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\CompanyPlace;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CompaniesController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['add-company', 'edit-company', 'delete-company'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'companies');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companiesData = $this->data()->getData();
        $companies = $companiesData->data;
        $recordsTotal = $companiesData->recordsTotal;
        return view('admin.companies', compact('companies', 'recordsTotal'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $isOrder = \request()->get('order');

        if ($isOrder) $query = Company::query();
        else $query = Company::query()->orderBy('id','desc')->limit(10);

        return Datatables::of($query)
            ->editColumn('image', function (Company $company) {
                return img(url('image/company/' . $company->image), "", "width='75px'");
            })
            ->addColumn('places', function (Company $company) {
                $places = $company->places()->with('city', 'neighborhood')->get();
                $back = "";
                foreach ($places as $place) {
                    $back .= '<i class="fa fa-location-arrow"></i> ' . $place->neighborhood->name . ', ' .
                        $place->city->name . '<br />';
                }
                return $back;
            })
            ->addColumn('user', function (Company $company) {
                return $company->user->name;
            })
            ->addColumn('options', function (Company $company) {

                $back = "";

                if (can(['edit-company', 'delete-company'])) {
                    if ( can('edit-company') ) $back .= data_edit_btn($company);
                    if ( can('delete-company') ) $back .= data_delete_btn($company);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options', 'places', 'name', 'image'])
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('add-company')) return no_permission();

        $this->validate($request, [
            'name'          => 'required|max:255',
            'description'       => 'required',
            'user_id'       => 'required',
        ]);

        $company = new Company();
        $company->name = $request->name;
        $company->description = $request->description;
        $company->user_id = $request->user_id;

        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $path = $request->file->storePublicly('companies');
                if( $path ) {
                    $company->image = str_replace('companies/', '', $path);
                }
            }
        } else {
            return response()->json([
                'errors' => [
                    'file' => 'Please choose the image',
                ]
            ], 422);
        }

        $company->save();

        if($request->cities) {
            $i = 0;
            foreach ($request->cities as $city) {
                CompanyPlace::create([
                    'company_id' => $company->id,
                    'city_id'   => $city,
                    'neighborhood_id'   => $request->neighborhoods[$i],
                ]);
                $i++;
            }
        }

        return $company;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $company = Company::with('places.city', 'places.neighborhood')->findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $company
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-company')) return no_permission();

        $this->validate($request, [
            'name'          => 'required|max:255',
            'description'       => 'required',
            'user_id'       => 'required',
        ]);

        $company = Company::find($id);
        $company->name = $request->name;
        $company->description = $request->description;
        $company->user_id = $request->user_id;

        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $path = $request->file->storePublicly('companies');
                if( $path ) {
                    $company->image = str_replace('companies/', '', $path);
                }
            }
        }
        $company->save();

        $ids = [];
        if($request->cities) {
            $i = 0;
            foreach ($request->cities as $city) {
                $cp = CompanyPlace::where([
                    'company_id' => $company->id,
                    'city_id' => $city,
                    'neighborhood_id' => $request->neighborhoods[$i],
                ])->first();
                if ($cp) {
                    $ids[] = $cp->id;
                }
                $i++;
            }
        }

        CompanyPlace::whereCompanyId($company->id)
            ->whereNotIn('id', $ids)
            ->delete();

        if($request->cities) {
            $i = 0;
            foreach ($request->cities as $city) {
                $cp = CompanyPlace::where([
                    'company_id' => $company->id,
                    'city_id' => $city,
                    'neighborhood_id' => $request->neighborhoods[$i],
                ])->first();
                if($cp) {
                    $i++;
                    continue;
                }
                CompanyPlace::create([
                    'company_id' => $company->id,
                    'city_id'   => $city,
                    'neighborhood_id'   => $request->neighborhoods[$i],
                ]);
                $i++;
            }
        }

        return $company;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (!auth()->user()->can('delete-company')) return no_permission();

        try{
            $company = Company::findOrFail($id);
            $company->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }
}

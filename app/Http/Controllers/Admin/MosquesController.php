<?php

namespace App\Http\Controllers\Admin;

use App\Mosque;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class MosquesController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['add-mosque', 'edit-mosque', 'delete-mosque'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'mosques');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mosquesData = $this->data()->getData();
        $mosques = $mosquesData->data;
        $recordsTotal = $mosquesData->recordsTotal;
        return view('admin.mosques', compact('mosques', 'recordsTotal'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $isOrder = \request()->get('order');

        if ($isOrder) $query = Mosque::query();
        else $query = Mosque::query()->orderBy('id','desc')->limit(10);

        return Datatables::of($query)
            ->editColumn('image', function (Mosque $mosque) {
                return img(url('image/mosque/' . $mosque->image), "", "width='75px'");
            })
            ->addColumn('city', function (Mosque $mosque) {
                return $mosque->city->name;
            })
            ->addColumn('neighborhood', function (Mosque $mosque) {
                return $mosque->neighborhood->name;
            })
            ->addColumn('options', function (Mosque $mosque) {

                $back = "";

                if (can(['edit-mosque', 'delete-mosque'])) {
                    if ( can('edit-mosque') ) $back .= data_edit_btn($mosque);
                    if ( can('delete-mosque') ) $back .= data_delete_btn($mosque);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options', 'image', 'name'])
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('add-mosque')) return no_permission();

        $this->validate($request, [
            'name'          => 'required|max:255',
            'description'       => 'required',
            'city_id'       => 'required|exists:cities,id',
            'neighborhood_id'       => 'required|exists:neighborhoods,id',
            'address'       => 'required',
        ]);

        $mosque = new Mosque();
        $mosque->name = $request->name;
        $mosque->description = $request->description;
        $mosque->city_id = $request->city_id;
        $mosque->neighborhood_id = $request->neighborhood_id;
        $mosque->address = $request->address;
        $mosque->latitude = $request->latitude;
        $mosque->longitude = $request->longitude;


        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $path = $request->file->storePublicly('mosques');
                if( $path ) {
                    $mosque->image = str_replace('mosques/', '', $path);
                }
            }
        } else {
            return response()->json([
                'errors' => [
                    'file' => 'Please choose the image',
                ]
            ], 422);
        }
        $mosque->save();

        return $mosque;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $mosque = Mosque::findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $mosque
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-mosque')) return no_permission();

        $this->validate($request, [
            'name'          => 'required|max:255',
            'description'       => 'required',
            'city_id'       => 'required|exists:cities,id',
            'neighborhood_id'       => 'required|exists:neighborhoods,id',
            'address'       => 'required',
        ]);

        $mosque = Mosque::find($id);
        $mosque->name = $request->name;
        $mosque->description = $request->description;
        $mosque->city_id = $request->city_id;
        $mosque->neighborhood_id = $request->neighborhood_id;
        $mosque->address = $request->address;
        $mosque->latitude = $request->latitude;
        $mosque->longitude = $request->longitude;
        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $path = $request->file->storePublicly('mosques');
                if( $path ) {
                    $mosque->image = str_replace('mosques/', '', $path);
                }
            }
        }
        $mosque->save();

        return $mosque;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (!auth()->user()->can('delete-mosque')) return no_permission();

        try{
            $mosque = Mosque::findOrFail($id);
            $mosque->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }
}

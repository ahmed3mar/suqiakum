<?php

namespace App\Http\Controllers\AuthAdmin;

use App\Center;
use App\City;
use App\Doctor;
use App\Place;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        Validator::extend('city_id', function ($attribute, $value, $parameters, $validator) {
            $user_type = request()->get('user_type') ? 'doctor' : 'user';
            if( $user_type == 'doctor' ) {
                $city = City::find($value);
                if( !$city ) return false;
            }
            return true;
        });

        Validator::extend('place_id', function ($attribute, $value, $parameters, $validator) {
            $user_type = request()->get('user_type') ? 'doctor' : 'user';
            if( $user_type == 'doctor' ) {
                $city = Place::find($value);
                if( !$city ) return false;
            }
            return true;
        });

        Validator::extend('center_id', function ($attribute, $value, $parameters, $validator) {
            $user_type = request()->get('user_type') ? 'doctor' : 'user';
            if( $user_type == 'doctor' ) {
                $city = Center::find($value);
                if( !$city ) return false;
            }
            return true;
        });

        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'user_type' => 'required',
            'city_id' => 'city_id',
            'place_id' => 'place_id',
            'center_id' => 'center_id',
            'password' => 'required|min:6|confirmed',
            'terms' => 'required',
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {

        $user_type = isset($data['user_type']) && $data['user_type'] == 'doctor' ? 'doctor' : 'user';

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'user_type' => $user_type,
            'password' => bcrypt($data['password']),
        ]);

        return $user;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CitiesController extends Controller
{

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!can(['add-city', 'edit-city', 'delete-city'])) {
                return redirect(route('admin.main'));
            }
            return $next($request);
        });

        \View::share('current', 'cities');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $citiesData = $this->data()->getData();
        $cities = $citiesData->data;
        $recordsTotal = $citiesData->recordsTotal;
        return view('admin.cities', compact('cities', 'recordsTotal'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $isOrder = \request()->get('order');

        if ($isOrder) $query = City::query();
        else $query = City::query()->orderBy('id','desc')->limit(10);

        return Datatables::of($query)
            ->addColumn('options', function (City $city) {

                $back = "";

                if (can(['edit-city', 'delete-city'])) {
                    if ( can('edit-city') ) $back .= data_edit_btn($city);
                    if ( can('delete-city') ) $back .= data_delete_btn($city);
                } else $back .= '-';

                return $back;
            })
            ->rawColumns(['options', 'image_url', 'name'])
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('add-city')) return no_permission();

        $this->validate($request, [
            'name'          => 'required|max:255',
        ]);

        $city = new City();
        $city->name = $request->name;
        $city->city_order = $request->city_order;

        $city->save();

        return $city;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $city = City::findOrFail($id);
            return response()->json([
                "success"   => true,
                "data"      => $city
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('edit-city')) return no_permission();

        $this->validate($request, [
            'name'          => 'required|max:255',
        ]);

        $city = City::find($id);
        $city->name = $request->name;
        $city->city_order = $request->city_order;
        $city->save();

        return $city;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (!auth()->user()->can('delete-city')) return no_permission();

        try{
            $city = City::findOrFail($id);
            $city->delete();
            return response()->json([
                "success"   => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "success"   => false,
                "message"   => __('admin.not_found')
            ]);
        }
    }
}

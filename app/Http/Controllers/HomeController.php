<?php namespace App\Http\Controllers;

use App\Company;
use App\Mosque;
use Intervention\Image\ImageManager;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( )
    {
        $companies = Company::limit(10)->get();

        $mosques = Mosque::with('city', 'neighborhood')->limit(10)->get();
        return view('home', compact('companies', 'mosques'));
    }

}

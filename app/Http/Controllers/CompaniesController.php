<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Company;

class CompaniesController extends Controller
{
    public function companies()
    {
        $companies = Company::paginate(15);
        return view('companies', compact('companies'));
    }

    public function company($id)
    {
        $company = Company::select('id', 'name', 'image', 'description')->findOrFail($id);
        return view('company', compact('company'));
    }

}

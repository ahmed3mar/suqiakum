<?php

namespace App\Http\Controllers;

use App\Company;
use App\Mosque;
use App\Order;
use App\Product;
use Illuminate\Http\Request;

class OrdersController extends Controller
{

    function index() {
        $companies = Company::all();
        return view('order', compact('companies'));
    }

    function data($id) {
        $company = Company::with('places')->find($id);
        $neighborhoods = [];
        foreach ($company->places as $place) {
            $neighborhoods[] = $place->neighborhood_id;
        }
        $mosques = Mosque::whereIn('neighborhood_id', $neighborhoods)->select('id', 'name', 'address')->get();

        if($company) {
            return [
                'status'    => 'ok',
                'result'    => [
                    'company'   => $company,
                    'mosques'   => $mosques,
                    'products'  => $company->products()->select('id', 'name', 'price')->get(),
                ]
            ];
        }

        return [
            'status'    => 'fail',
            'reason'    => 'company_not_found'
        ];
    }

    function company($id) {
        $company = Company::with('places')->findOrFail($id);
        $neighborhoods = [];
        foreach ($company->places as $place) {
            $neighborhoods[] = $place->neighborhood_id;
        }
        $mosques = Mosque::whereIn('neighborhood_id', $neighborhoods)->select('id', 'name', 'address')->get();
        return view('order', compact('company', 'mosques'));
    }

    function product($id) {
        $product = Product::with('company.places')->findOrFail($id);
        $company = $product->company;
        $neighborhoods = [];
        foreach ($company->places as $place) {
            $neighborhoods[] = $place->neighborhood_id;
        }
        $mosques = Mosque::whereIn('neighborhood_id', $neighborhoods)->select('id', 'name', 'address')->get();
        return view('order', compact('product', 'company', 'mosques'));
    }

    function mosque($id) {
        $mosque = Mosque::findOrFail($id);
        $companies = $mosque->neighborhood->companies;
        return view('order', compact('mosque', 'companies'));
    }

    function store(Request $request) {

        $this->validate($request, [
            'company_id'    => 'required|exists:companies,id',
            #'product_id'    => 'required|exists:products,id',
            #'quantity'      => 'required|numeric|min:1',
            'products'      => 'required',
            'mosque_id'     => 'required|exists:mosques,id',
            'phone'         => 'required|numeric',
            'name'          => 'required|max:255',
            'address'       => 'required|max:255',
        ]);

        # Check if valid products
        if(isset($request->products) && sizeof($request->products) > 0) {
            foreach ($request->products as $product) {
                if ( !isset($product['product_id']) or !isset($product['quantity'])) {
                    return response()->json([
                        'errors' => [
                            'products' => 'Please enter valid object',
                        ]
                    ], 422);
                }
                $_product = Product::find($product['product_id']);
                if (!$_product) {
                    return response()->json([
                        'errors' => [
                            'products' => 'Product of id ' . $product['product_id'] . ' Not found',
                        ]
                    ], 422);
                }
            }
        } else {
            return response()->json([
                'errors' => [
                    'products' => 'Please choose your products',
                ]
            ], 422);
        }

        $order = new Order();
        #$order->company_id = $request->company_id;
        #$order->product_id = $request->product_id;
        #$order->quantity = $request->quantity;
        $order->products = json_encode($request->products);
        $order->mosque_id = $request->mosque_id;
        $order->phone = $request->phone;
        $order->name = $request->name;
        $order->address = $request->address;
        $order->save();

        return [
            'status'    => 'ok',
//            'result'    =>
        ];

    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mosque;

class MosquesController extends Controller
{
    public function mosques()
    {
        $mosques = Mosque::with('city', 'neighborhood')->paginate(15);
        return view('mosques', compact('mosques'));
    }

    public function mosque($id)
    {
        $mosque = Mosque::select('id', 'image', 'name', 'description', 'address')->findOrFail($id);
        return view('mosque', compact('mosque'));
    }

}

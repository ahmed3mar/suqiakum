<?php

use App\Option;
static $options = [];

function parseOptions() {
    global $options;

    $options = Option::all()->pluck('value', 'key');
    return $options;
}

function option( $key ) {
    global $options;

    if (!$options || sizeof($options) == 0) $options = parseOptions();

    return $options->get($key);

}

function admin_resource($route, $controller, $name) {
    Route::post($route . '/data', $controller . '@data')->name('.' . $name . '.data');
    Route::resource($route, $controller)->names([
        'index' => '.' . $name
    ]);
}

# Translations
function _trans($key) {
    return \Lang::locale() == 'ar' ? $key . '_ar' : $key;
}

# Permissions
function list_permissions() {
    $permissionsData = \App\Permission::all();
    $permissionsGroups = [];
    foreach ($permissionsData as $permission) {
        if (!isset($permissionsGroups[$permission->category]))
            $permissionsGroups[$permission->category] = [];

        $permissionsGroups[$permission->category][] = [
            'name'  => $permission->name,
            'id'    => $permission->id,
        ];
    }
    return $permissionsGroups;
}

function no_permission() {
    return response()->json([
        'success'   => false,
        'message'   => 'no-permission'
    ], 403);
}

function can($permission) {
    return auth()->user()->can($permission);
}

// Due to repeating in each Controller in Admin
function data_edit_btn($model) {
    $back = "&nbsp;&nbsp;";
    $back .= '<a href="#" data-id="' . $model->id . '" return false;" title="Edit" class="edit-item tooltips"><i class="fa fa-1-8x fa-edit"></i></a>';
    return $back;
}

function data_btn($model, $title, $class, $icon) {
    $back = "&nbsp;&nbsp;";
    $back .= '<a href="#" data-id="' . $model->id . '" return false;" title="'.$title.'" class="'.$class.' tooltips">'.$icon.'</a>';
    return $back;
}

function data_delete_btn($model, $text = "", $icon = '<i class="fa fa-times fa-1-8x text-danger"></i>') {
    $back = $text ? "" : "&nbsp;&nbsp;";
    $back .= '<a href="#"
                        class="delete-item"
                        data-id="' . $model->id . '"
                        data-toggle="confirmation"
                        data-placement="top"
                        data-btnOkLabel="<i class=\'icon-ok-sign icon-white\'></i> Yes"
                        data-btnCancelLabel="<i class=\'icon-remove-sign\'></i>  No"
                        data-title="Are you sure to delete?" data-target="_self"
                        >' . $icon.$text.'</a>';
    return $back;
}

function img($url, $alt = '', $attrs = "") {
    return '<img src="' . $url . '" alt="'.$alt.'" '.$attrs.' />';
}
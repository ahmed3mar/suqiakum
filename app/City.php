<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    function companies() {
        return $this->belongsToMany(Company::class, 'company_places');
    }

    function mosques() {
        return $this->hasMany(Mosque::class);
    }

    function neighborhoods() {
        return $this->hasMany(Neighborhood::class);
    }
}

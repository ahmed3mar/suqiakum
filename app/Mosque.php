<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mosque extends Model
{
    function city() {
        return $this->belongsTo(City::class);
    }

    function neighborhood() {
        return $this->belongsTo(Neighborhood::class);
    }


    function getImageUrlAttribute() {
        return route('image', ['mosque', $this->image]);
    }

    function getUrlAttribute() {
        return route('mosque', $this->id);
    }

    function getOrderUrlAttribute() {
        return route('order.mosque', $this->id);
    }

}

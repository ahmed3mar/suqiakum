<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPlace extends Model
{
    protected $fillable = ['company_id', 'city_id', 'neighborhood_id'];

    function city() {
        return $this->belongsTo(City::class);
    }

    function neighborhood() {
        return $this->belongsTo(Neighborhood::class);
    }
}

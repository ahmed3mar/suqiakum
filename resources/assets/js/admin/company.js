var Company = function () {

    var dataTable;
    let current_neighborhood_id = 0;

    function init() {

        dataTable = $('#datatable').DataTable({
            "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "processing": true,
            "serverSide": true,
            // "deferLoading": $('#datatable').attr("data-total"),
            "ajax": {
                url: pageInfo.ajax,
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': window.Laravel.csrfToken,
                    'X-Requested-With': 'XMLHttpRequest'
                }
            },
            "columns": [
                { "data": "id"},
                { "data": "image", width: "20%"},
                { "data": "name", width: "30%"},
                { "data": "places", width: "20%"},
                { "data": "user", width: "10%"},
                { "data": "options", searchable: false, orderable: false, "class": "text-center" }
            ],
            "order" : [
                [0, "desc"]
            ],
            "drawCallback": function( settings ) {
                $("[data-toggle=tooltip],.tooltips").tooltip();
            }
        });

        $(document).on('change', '#city_id', function () {
            // get Neighborhoods
            axios.get(
                config.admin_url + '/neighborhoods/list/' + $('#city_id').val()
            ).then( res => {
                const data = res.data;
                $('#neighborhood_id').html("");
                for( let x in data ) {
                    $('#neighborhood_id').append(`
                        <option value="${x}">${data[x]}</option>
                    `);
                }

                if (current_neighborhood_id) {
                    $('#neighborhood_id').val(current_neighborhood_id);
                }
            })
            return false;
        });

        $(document).on('click', '.delete-place', function () {
            $(this).parent().parent().remove();
            return false;
        });

        $('#add_place').click(function () {
            if($('#city_id').val() && $('#neighborhood_id').val()) {
                const city_id = $('#city_id').val();
                const neighborhood_id = $('#neighborhood_id').val();
                const city = $('#city_id option:selected').text();
                const neighborhood = $('#neighborhood_id option:selected').text();
                $('#places').append(`
                    <tr>
                    <td>${city}<input type="hidden" name="cities[]" value="${city_id}"></td>
                    <td>${neighborhood}<input type="hidden" name="neighborhoods[]" value="${neighborhood_id}"></td>
                    <td><a href="#" class="btn btn-danger btn-sm delete-place">DELETE</a></td>
</tr>
                `)
            }
            return false;
        });

        $('#addNewItem').click(function () {
            Company.add();
        });

        $(document).on('click', '.edit-item', function () {
            Company.edit(this);
            return false;
        });

        $(document).on("click", ".delete-item", function () {
            Company.delete(this);
            return false;
        });

        if (!jQuery().validation) {
            return;
        }

        $('#item-form').validation({
            type: 'normal'
        });

        if( $('#save-item').length )
        {
            $('#item-form').submit(function () {

                $('.has-error').removeClass('has-error')
                $('.submit-form').html('<i class="fa fa-spin fa-spinner"></i> Save').prop('disabled', true);

                var data = new FormData();
                data.append('id', $('#id').val());
                data.append('name', $('#name').val());
                data.append('description', $('#description').val());
                data.append('user_id', $('#user_id').val());

                $('[name="cities[]"]').each(function () {
                    data.append('cities[]', $(this).val());
                });

                $('[name="neighborhoods[]"]').each(function () {
                    data.append('neighborhoods[]', $(this).val());
                });

                data.append('file', document.getElementById('file').files[0]);

                let req;
                if( $('#id').val() != '0' )
                {
                    data.append('_method', 'PUT');
                    let action = config.admin_url + '/' + pageInfo.slug + '/' + $('#id').val();
                    req = axios.post(action, data)
                        .then( res => {
                            $('#addEditModal').modal('hide');
                            showToastr('success', 'Company updated successfully');
                            dataTable.ajax.reload( null, false );
                            $('.submit-form').html('Save').prop('disabled', false);
                        })
                } else {
                    let action = config.admin_url + '/' + pageInfo.slug;
                    req = axios.post(action, data)
                        .then( res => {
                            $('#addEditModal').modal('hide');
                            showToastr('success', 'Company added successfully');
                            dataTable.ajax.reload( null, false );
                            $('.submit-form').html('Save').prop('disabled', false);
                        })
                }

                req
                    .catch( err => {
                        if( err.response ) {
                            let errors = err.response.data;
                            parseErrors(errors);
                        }
                        $('.submit-form').html('Save').prop('disabled', false);
                    });

                return false;
            })
        }

    }

    return {
        init: init,

        clear: function () {
            // clear the form inputs and textareas !
            $('#item-form').find('input, textarea').val("");

            // hide help-block and error messages
            $('.help-block').hide();
            $('.has-error').removeClass('has-error');

            $('#add-label').show();
            $('#edit-label').hide();

            // reset back the input #id
            $('#id').val(0);
            $('#city_id').val("0");
            $('#neighborhood_id').html("");
            current_neighborhood_id = 0;
        },

        add: function () {

            current_neighborhood_id = 0;
            Company.clear();
            $('#addEditModal').modal('show');

        },

        edit: function (t) { // t == this

            Company.clear();

            $('#edit-label').show();
            $('#add-label').hide();

            $(t).html(icons.loading);

            axios.get( config.admin_url + '/' + pageInfo.slug + '/' + $(t).data('id') )
                .then(res => {
                    const response = res.data;
                    if( response && response.success )
                    {
                        for( var i in response.data )
                        {
                            $('#' + i).val(response.data[i]).trigger('change');
                        }
                        current_neighborhood_id = response.data['neighborhood_id'];
                        const places = response.data['places'];
                        $('#places').html("");
                        for (let x in places) {
                            const city_id = places[x]['city'].id;
                            const neighborhood_id = places[x]['neighborhood'].id;
                            const city = places[x]['city'].name;
                            const neighborhood = places[x]['neighborhood'].name;
                            $('#places').append(`
                    <tr>
                    <td>${city}<input type="hidden" name="cities[]" value="${city_id}"></td>
                    <td>${neighborhood}<input type="hidden" name="neighborhoods[]" value="${neighborhood_id}"></td>
                    <td><a href="#" class="btn btn-danger btn-sm delete-place">DELETE</a></td>
</tr>
                `)
                        }
                        $('#addEditModal').modal('show');
                    }
                    $(t).html(icons.edit);
                })
                .catch( err => {
                    if( err.response ) {
                        parseErrors(err.response.data);
                    }
                    $(t).html(icons.edit);
                });

        },

        delete: function (t) {

            $(t).confirmation({
                href: "javascript:;",
                onConfirm: function () {

                    $(t).html(icons.loading);

                    axios.delete(config.admin_url + '/' + pageInfo.slug +'/' + $(t).data('id'))
                        .then( res => {
                            $(t).html(icons.delete);
                            showToastr('success', 'Company deleted successfully');
                            dataTable.ajax.reload( null, false );
                        })
                        .catch( err => {
                            if( err.response ) {
                                parseErrors(err.response.data);
                            }
                            $(t).html(icons.delete);
                        });

                    return false;
                }
            }).confirmation('show');

        }

    }

}();

Company.init();
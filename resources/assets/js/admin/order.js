var Order = function () {

    var dataTable;

    function init() {

        dataTable = $('#datatable').DataTable({
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],

            "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            "processing": true,
            "serverSide": true,
            // "deferLoading": $('#datatable').attr("data-total"),
            "ajax": {
                url: pageInfo.ajax,
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': window.Laravel.csrfToken,
                    'X-Requested-With': 'XMLHttpRequest'
                }
            },
            "columns": [
                { "data": "id"},
                { "data": "ad_title", width: "20%"},
                { "data": "orders", width: "10%"},
                { "data": "brand_id", width: "10%"},
                { "data": "year", width: "10%"},
                { "data": "name", width: "15%"},
                { "data": "email", width: "15%"},
                { "data": "status", width: "10%"},
                { "data": "comment", width: "10%"},
                { "data": "created_at", width: "10%"},
                { "data": "options", searchable: false, orderable: false, "class": "text-center" }
            ],
            "order" : [
                [0, "desc"]
            ],
            "drawCallback": function( settings ) {
                $("[data-toggle=tooltip],.tooltips").tooltip();
            }
        });

        Vue.component('orders-filter', require('./components/OrdersFilter.vue'));
        new Vue({
            el: '#vue-components'
        });

        $(document).on('click', '#filterIt', () => {

            const query = $('#query').val();
            const brand = $('#brand').val();
            const city = $('#city').val();
            const model = $('#model').val();
            const transmission_type = $('#transmission_type').val();
            const kilometer = $('#kilometer').val();
            const status = $('#status').val();
            const date_from = $('#date-from').val();
            const date_to = $('#date-to').val();

            let queries = "";
            // if ( $('#query').val() )
            //     queries += 'q=' + $('#query').val() + '&';
            //
            // if ( query )
            //     queries += 'q=' + query + '&';
            //
            if ( brand )
                queries += 'brand=' + brand + '&';

            if ( model )
                queries += 'model=' + model + '&';

            if ( transmission_type )
                queries += 'transmission=' + transmission_type + '&';

            if ( kilometer )
                queries += 'kilometer=' + kilometer + '&';

            if ( status )
                queries += 'status=' + status + '&';

            if ( date_from )
                queries += 'date_from=' + date_from + '&';

            if ( date_to )
                queries += 'date_to=' + date_to + '&';

            if ( city )
                queries += 'city=' + city + '&';


            dataTable.ajax.url(pageInfo.url + queries).load();

        });


        $(document).on('click', '#exportIt', () => {

            const brand = $('#brand').val();
            const city = $('#city').val();
            const model = $('#model').val();
            const transmission_type = $('#transmission_type').val();
            const kilometer = $('#kilometer').val();
            const status = $('#status').val();
            const date_from = $('#date-from').val();
            const date_to = $('#date-to').val();

            let queries = "";

            if ( brand )
                queries += 'brand=' + brand + '&';

            if ( model )
                queries += 'model=' + model + '&';

            if ( transmission_type )
                queries += 'transmission=' + transmission_type + '&';

            if ( kilometer )
                queries += 'kilometer=' + kilometer + '&';

            if ( status )
                queries += 'status=' + status + '&';

            if ( date_from )
                queries += 'date_from=' + date_from + '&';

            if ( date_to )
                queries += 'date_to=' + date_to + '&';

            if ( city )
                queries += 'city=' + city + '&';

            queries += 'type=' + $('#type').val();

            // console.log  ( config.admin_url + '/' + pageInfo.slug + '/export?' + queries );
            location.href = ( config.admin_url + '/' + pageInfo.slug + '/export?' + queries );
            // dataTable.ajax.url(pageInfo.url + queries).load();

        });

        $(document).on('click', '[name="order_option"]', function () {
            const option = $(this).val();
            const id = $(this).data('id');
            if (option === 'sold-out' || option === 'pending' || option === 'delete' ) {
                $(this).prop('disabled', true);

                axios.put( config.admin_url + '/' + pageInfo.slug + '/' + id, {
                    type: option,
                })
                    .then( res => {
                        showToastr('success', 'order updated successfully');
                        dataTable.ajax.reload( null, false );
                    })

            } else if (option == 'view') {
                Order.view(this);
            }
            return false;
        });

        $(document).on('click', '.view-order', function () {
            Order.view(this);
            $('.accept-order-btn, .cancel-order-btn').prop('disabled', false);
            return false;
        });

        $(document).on('click', '.edit-comment', function () {
            Order.view(this);
            return false;
        });

        $('#save-comment').click(function () {
            const id = $('#id').val();
            axios.put( config.admin_url + '/' + pageInfo.slug + '/' + id, {
                type: 'comment',
                comment: $('#comment').val()
            })
                .then( res => {
                    console.log("sss")
                    showToastr('success', 'order updated successfully');
                    $('#viewModal').modal('hide');
                    dataTable.ajax.reload( null, false );
                })
            return false;
        })

        $(document).on('click', '.accept-order', function () {
            Order.status(this, 'accept');
            return false;
        });

        $(document).on('click', '.delete-order', function () {
            Order.delete(this);
            return false;
        });

        $(document).on('click', '.cancel-order', function () {
            Order.status(this, 'cancel');
            return false;
        });

        $(document).on('click', '.accept-order-btn', function () {
            Order.status(this, 'accept');
            return false;
        });

        $(document).on('click', '.cancel-order-btn', function () {
            Order.status(this, 'cancel');
            return false;
        });

    }

    return {
        init: init,

        view: function (t) {

            // $(t).html(icons.loading);

            axios.get( config.admin_url + '/' + pageInfo.slug + '/' + $(t).data('id') )
                .then(res => {
                    const response = res.data;
                    if( response && response.success )
                    {
                        $('#id').val(response.data['id'])
                        $('#comment').val(response.data['comment'])

                        for( var i in response.data )
                        {
                            $('td[data-id="'+ i +'"]').html(response.data[i]);
                        }
                        $('.accept-order-btn').text('Accept');
                        $('.cancel-order-btn').text('Cancel');

                        $('.accept-order-btn, .cancel-order-btn').data('id', response.data['id']);

                        if (response.data['status'] === 'pending' ) {
                            $('.accept-order-btn, .cancel-order-btn').show();
                        } else {
                            $('.accept-order-btn, .cancel-order-btn').hide();
                        }

                        $('#viewModal').modal('show');
                    }
                    // $(t).html(icons.view);
                })
                .catch( err => {
                    if( err.response ) {
                        parseErrors(err.response.data);
                    }
                    // $(t).html(icons.view);
                });

        },

        delete: function (t) {

            $(t).confirmation({
                href: "javascript:;",
                onConfirm: function () {

                    $(t).html(icons.loading);

                    axios.delete(config.admin_url + '/' + pageInfo.slug +'/' + $(t).data('id'))
                        .then( res => {
                            $(t).html(icons.delete);
                            showToastr('success', 'Order deleted successfully');
                            dataTable.ajax.reload( null, false );
                        })
                        .catch( err => {
                            if( err.response ) {
                                parseErrors(err.response.data);
                            }
                            $(t).html(icons.delete);
                        });

                    return false;
                }
            }).confirmation('show');

        },

        status: function (t, type = 'accept') {

            if (type !== 'accept' && type !== 'cancel') return;

            $(t).confirmation({
                href: "javascript:;",
                onConfirm: function () {

                    $(t).html(icons.loading);

                    axios.put(config.admin_url + '/' + pageInfo.slug +'/' + $(t).data('id'), {
                        type
                    })
                        .then( res => {
                            $(t).html(type === "accept" ? icons.check : icons.delete);
                            showToastr('success', 'Order updated successfully');
                            $('#viewModal').modal('hide');
                            dataTable.ajax.reload( null, false );
                        })
                        .catch( err => {
                            if( err.response ) {
                                parseErrors(err.response.data);
                            }
                            $(t).html(type === "accept" ? icons.check : icons.delete);
                        });

                    return false;
                }
            }).confirmation('show');

        }

    }

}();

Order.init();
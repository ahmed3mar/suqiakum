const Options = function () {

    function init() {

        $('#submit-global').prop('disabled', false);
        $('#submit-close').prop('disabled', false);

        $('[name="site_close"]').change(function () {
           if ($(this).val() == 0) {
               $('#site_close_message').prop('disabled', true);
               $('#site_close_message_ar').prop('disabled', true);
           } else {
               $('#site_close_message').prop('disabled', false);
               $('#site_close_message_ar').prop('disabled', false);
           }
        });

        if ($('[name="site_close"]:checked').val() == 0) {
            $('#site_close_message').prop('disabled', true);
            $('#site_close_message_ar').prop('disabled', true);
        }

        if (!jQuery().validation) {
            return;
        }

        // Init the validation
        $('#options-global').validation({type: 'normal'});
        $('#options-close').validation({type: 'normal'});

        // On click event for the forms !
        $('#submit-global').click(() => {
            if ($('#options-global').validate().form()) {
                $('#options-global').submit(); //form validation success, call ajax form submit
            }
            return false;
        });

        $('#submit-close').click(() => {
            if ($('#options-close').validate().form()) {
                $('#options-close').submit(); //form validation success, call ajax form submit
            }
            return false;
        });

        $('#options-global,#options-close').submit(function () {

            $('.has-error').removeClass('has-error');
            $('.help-block:not(.keep)').hide();

            // So it's global form =D
            const req = axios.post( pageInfo.ajax , $(this).serialize())
                .then( res => {
                    showToastr('success', 'Options updated successfully');
                }).catch( err => {

                    if( err.response ) {
                        let errors = err.response.data;
                        if (err.response.status == 500) {
                            showToastr('error', 'Unknown error!');
                        } else {
                            for( let i in errors )
                            {
                                $('[name="' + i + '"]')
                                    .closest('.form-group').addClass('has-error').removeClass("has-info");
                                if( !$('#' + i).parent().find(".help-block").length )
                                {
                                    $('#' + i).parent().append('<span id="'+i+'-error" class="help-block help-block-error"></span>');
                                }
                                $('#' + i).parent().find(".help-block").html(errors[i]);
                            }
                        }
                    } else {
                        showToastr('error', 'Unknown error!');
                    }
                })

        });

    }

    return {
        init: init,

    }

}();

Options.init();
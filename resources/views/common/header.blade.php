<header id="header" class="header-narrow" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 50, 'stickySetTop': '-20px', 'stickyChangeLogo': false}">
    <div class="header-body">
        <div class="header-top header-top-secondary header-top-style-3">
            <div class="container">
            </div>
        </div>
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-logo">
                        <a href="{{ route('home') }}">
                            <img alt="{{ config('app.name') }}" width="150" src="{{ asset('img/logo-s.png') }}">
                        </a>
                    </div>
                </div>
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-nav">
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
                                <i class="fa fa-bars"></i>
                            </button>
                            <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                                <nav>
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li class="">
                                            <a class="" href="{{ route('home') }}">
                                                الرئيسية
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="{{ route('companies') }}">
                                                شركات المياه
                                            </a>
                                        </li>
                                        <li class="">
                                            <a class="" href="{{ route('mosques') }}">
                                                المساجد
                                            </a>
                                        </li>
                                        <li class="">
                                            <a class="" href="{{ route('order') }}">نموذج شراء</a>
                                        </li>
                                        <li class="">
                                            <a class="" href="contact.html">
                                                تواصل معنا
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
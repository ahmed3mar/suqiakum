<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="footer-ribbon">
                <span>تواصل معنا</span>
            </div>
            <div class="col-md-4">
                <div class="newsletter">
                    <h4>اشترك معنا</h4>
                    <p"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور</p>

                    <div class="alert alert-success hidden" id="newsletterSuccess">
                        <strong>Success!</strong> You've been added to our email list.
                    </div>

                    <div class="alert alert-danger hidden" id="newsletterError"></div>

                    <form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST">
                        <div class="input-group">
                            <input class="form-control" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">
                            <span class="input-group-btn">
											<button class="btn btn-default" type="submit">Go!</button>
										</span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-5">
                <div class="contact-details">
                    <h4>تواصل معنا</h4>
                    <ul class="contact">
                        <li><p><i class="fa fa-map-marker"></i> <strong>العنوان:</strong> 1234 Street Name, City Name, United States</p></li>
                        <li><p><i class="fa fa-phone"></i> <strong>التليفون:</strong> (123) 456-789</p></li>
                        <li><p><i class="fa fa-envelope"></i> <strong>الايميل:</strong> <a href="mailto:mail@example.com">mail@example.com</a></p></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <h4>تابعنا</h4>
                <ul class="social-icons">
                    <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                    {{--<a href="{{ route('home') }}" class="logo">--}}
                        {{--<img alt="Porto Website Template" class="img-responsive" src="img/logo-footer.png">--}}
                        {{--<img alt="{{ config('app.name') }}" width="150" src="{{ asset('img/logo-s.png') }}">--}}
                    {{--</a>--}}
                </div>
                <div class="col-md-7">
                    <p>© Copyright 2017. All Rights Reserved.</p>
                </div>
                <div class="col-md-4">
                    <nav id="sub-menu">
                        <ul>
                            <li><a href="page-faq.html">FAQ's</a></li>
                            <li><a href="sitemap.html">Sitemap</a></li>
                            <li><a href="contact-us.html">Contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</footer>
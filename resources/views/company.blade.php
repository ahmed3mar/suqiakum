@extends('layouts.app')
@section('content')

	<section class="page-header text-left">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb text-left">
						<li><a href="#">الرئيسية</a></li>
						<li class="active">عروض شركة المياه</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-left">
					<h1 class="text-left">عروض شركة المياه</h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<hr class="tall">
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">

				<div class="thumbnail">
					<img src="{{ $company->image_url }}" class="img-responsive img-rounded" alt="">
				</div>
			</div>

			<div class="col-md-6">

				<div class="summary entry-summary">

					<h1 class="mb-none"><strong>{{ $company->name }}</strong></h1>

					<hr class="tall">
					<p class="taller">{{ $company->description }}</p>
					<a href="{{ $company->order_url }}" class="btn btn-primary btn-icon">طلب شراء</a>



				</div>


			</div>
		</div>


		<div class="row">
			<div class="col-md-12">
				<hr class="tall">

				<h4 class="mb-md text-uppercase">عروض <strong>الأسعار</strong></h4>

				<div class="row">

					<ul class="products product-thumb-info-list">
						@foreach($company->products as $product)
							<li class="col-md-3 col-sm-6 col-xs-12 product">
								<span class="product-thumb-info">
									<a href="{{ $product->url }}">
										<span class=""><img src="{{ $product->image_url }}" class="img-responsive" alt=""></span>
									</a>
									<span class="product-thumb-info-content text-center">
										<a href="{{ $product->url }}" class="text-center">
										<span class="thumb-info-caption-title text-uppercase text-color-dark font-weight-bold">{{ $product->name }}</span>
										<p>{{ $product->description }}</p>
											<a class="btn btn-primary" href="{{ $product->order_url }}">{{ $product->price }} ريال</a>
										</a>
									</span>
								</span>
							</li>
						@endforeach
					</ul>

				</div>
			</div>
		</div>
	</div>


@endsection

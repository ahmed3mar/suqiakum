@extends('layouts.app')
@section('content')
    <div class="slider-container rev_slider_wrapper">
        <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 500}">
            <ul>
                <li data-transition="fade">

                    <img src="img/water/369019.jpg"
                         alt=""
                         data-bgposition="center center"
                         data-bgfit="cover"
                         data-bgrepeat="no-repeat"
                         class="rev-slidebg">


                </li>
                <li data-transition="fade">

                    <img src="img/water/glass-of-water-hd-wallpaper-1024x576.jpg"
                         alt=""
                         data-bgposition="center center"
                         data-bgfit="cover"
                         data-bgrepeat="no-repeat"
                         class="rev-slidebg">


                </li>
                <li data-transition="fade">

                    <img src="img/water/330491.jpg"
                         alt=""
                         data-bgposition="center center"
                         data-bgfit="cover"
                         data-bgrepeat="no-repeat"
                         class="rev-slidebg">


                </li>
            </ul>
        </div>
    </div>
    <section class="section section-default section-default-scale-8 no-margins">
        <div class="container">
            <div class="row">
                <div class="col-md-12 center">
                    <h2 class="mb-none mt-none font-weight-semibold text-light">من نحن</h2>
                    <p class="lead mb-xlg">شركة سقياكم لخدمات المياه </p>
                    <div class="divider divider-primary divider-small divider-small-center mb-xl">
                        <hr>
                    </div>
                    <p class="mb-none text-light">"لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور

                        أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد

                        أكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس
                        أكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس

                        أيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت.</p>

                    <a class="btn btn-primary mt-xl mb-sm" href="#">إقرأ المزيد <i class="fa fa-angle-right pl-xs"></i></a>
                </div>
            </div>
        </div>
    </section>
    <section dir="ltr" id="cases" class="section section-no-border  m-none">
        <div class="container">
            <div class="row center">
                <div class="col-md-12">
                    <h2>شركات المياه</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel show-nav-title custom-arrows-style-1" data-plugin-options="{'responsive': {'767': {'items': 1}, '1200': {'items': 3}}, 'margin': 15, 'loop': false, 'dots': false, 'nav': true}">
                        @foreach($companies as $company)
                        <div class="center">
                            <article class="thumb-info custom-thumb-info-style-2">
											<span class="thumb-info-wrapper m-none">
												<a href="{{ $company->url }}" class="text-decoration-none" title="Porto Website">
													<img src="{{ $company->image_url }}" class="img-responsive img-main" alt="">
												</a>
											</span>
                                <span class="thumb-info-caption background-color-light center p-md">
												<h4>
													<a href="{{ $company->url }}" class="text-decoration-none text-color-dark">{{ $company->name }}</a>
												</h4>
												{{--<p class="custom-thumb-info-category">معلومات عن شركة المياه</p>--}}
											</span>
                            </article>
                            <a href="{{ $company->url }}" class="btn custom-btn-style-1 btn-primary mt-md">اسعار الشركة</a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section section-no-border custom-section-spacement-1 background-color-light m-none">
        <div class="container">
            <div class="row center">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-color-dark font-weight-bold">المساجد</h2>
                </div>
            </div>
            <div class="row">
                @foreach($mosques as $mosque)
                <div class="col-md-3 col-sm-6">
                    <a href="{{ $mosque->url }}" class="popup-with-zoom-anim text-decoration-none">
								<span class="thumb-info custom-thumb-info-1 thumb-info-no-borders">
									<span class="thumb-info-wrapper p-none">
										<img src="{{ $mosque->image_url }}" class="img-responsive" alt="">
									</span>
									<span class="thumb-info-caption background-color-light">
										<span class="thumb-info-caption-text">
											<span class="thumb-info-caption-title text-uppercase text-color-dark font-weight-bold">{{ $mosque->name }}</span>
											<p>{{ $mosque->city->name }} - {{ $mosque->neighborhood->name }}</p>
											<span class="custom-thumb-info-caption-plus background-color-primary text-color-light">+</span>
										</span>
									</span>
								</span>
                    </a>
                    <div id="team-content-2" class="dialog dialog-lg zoom-anim-dialog mfp-hide p-xlg">
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <img src="{{ $mosque->image_url  }}" class="img-responsive mb-lg" alt="">
                                <ul class="social-icons custom-social-icons center">
                                    <li class="social-icons-facebook">
                                        <a href="http://www.facebook.com/" target="_blank" title="Facebook">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="social-icons-twitter">
                                        <a href="http://www.twitter.com/" target="_blank" title="Twitter">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li class="social-icons-google">
                                        <a href="http://www.plus.google.com/" target="_blank" title="Twitter">
                                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li class="social-icons-linkedin">
                                        <a href="http://www.linkedin.com/" target="_blank" title="Linkedin">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <h2 class="font-weight-semibold text-color-dark mb-xs">John Doe</h2>
                                <p class="custom-font-style-1 font-weight-semibold">Founder of Porto</p>
                                <p class="font-size-sm custom-line-height-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at elementum lacus. Fusce luctus urna ac mauris consequat, ac eleifend odio imperdiet. Sed euismod tempor orci, ullamcorper accumsan justo semper eu. Donec venenatis elit et euismod iaculis. Integer vehicula imperdiet metus at convallis. Donec ullamcorper at nunc lobortis ultricies. Nam tincidunt.</p>
                                <p class="font-size-sm custom-line-height-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In imperdiet faucibus congue. Donec at volutpat nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas risus dui.</p>
                                <p class="font-size-sm custom-line-height-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris pellentesque at purus et auctor. Donec eget risus vitae lacus aliquam porttitor. Morbi pellentesque fermentum ex, nec sagittis augue mollis sit amet. Pellentesque vitae metus porttitor ipsum finibus tincidunt. Praesent sit amet fringilla nisi, in porttitor eros. Vivamus iaculis mattis purus, vitae dictum felis tempor nec. Maecenas eget fermentum turpis. Fusce dignissim leo sit amet aliquet facilisis. Sed erat arcu, ultricies a justo vitae, elementum egestas justo. Nunc bibendum, ex id dapibus fringilla, nisl quam tincidunt ligula, at luctus quam magna nec sem. Suspendisse vitae tempus velit, nec congue sapien.</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-md-12 center">
                    <a href="{{ route('mosques') }}" class="btn btn-borders btn-primary custom-btn-style-2 font-weight-semibold text-color-dark text-uppercase mt-xlg">عرض المزيد</a>
                </div>
            </div>
        </div>
    </section>

    <!--
                    <div class="container">
                        <h1>طلب شراء</h1>
                        <form class="form-horizontal" novalidate="novalidate">
                            <div class="tab-content">
                                <div id="w1-profile" class="tab-pane active">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="w1-first-name">اسم العميل</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control input-sm" name="first-name" id="w1-first-name" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="w1-last-name">رقم التليفون</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control input-sm" name="last-name" id="w1-last-name" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="w1-last-name">مكان التسليم</label>
                                        <div class="col-sm-8">
                                            <select name="cs" id="" class="form-control">
                                                <option value="">اسم المسجد</option>
                                                <option value="">اسم المسجد</option>
                                                <option value="">اسم المسجد</option>
                                                <option value="">اسم المسجد</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="w1-last-name">طريقة السداد</label>
                                        <div class="col-sm-8">
                                            <select name="cs"  class="form-control">
                                                <option value="">طريقة السداد</option>
                                                <option value="">طريقة السداد</option>
                                                <option value="">طريقة السداد</option>
                                                <option value="">طريقة السداد</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="w1-last-name">اختيار الصنف</label>
                                        <div class="col-sm-8">
                                            <select name="cs"  class="form-control">
                                                <option value="">صنف</option>
                                                <option value="">صنف</option>
                                                <option value="">صنف</option>
                                                <option value="">صنف</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="w1-last-name">اختيار الكمية</label>
                                        <div class="col-sm-8">
                                            <select name="cs"  class="form-control">
                                                <option value="">كمية</option>
                                                <option value="">كمية</option>
                                                <option value="">كمية</option>
                                                <option value="">كمية</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="w1-last-name">الوقت المقترح للتسليم</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                <input id="date" data-plugin-masked-input="" data-input-mask="99/99/9999" placeholder="__/__/____" class="form-control">
                                            </div>									</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary-scale-2 btn-lg">اطلب</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
    -->
@endsection

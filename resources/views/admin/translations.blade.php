@extends('admin.layouts.app')

@section('before_scripts')
    <script>
        var pageInfo = {
            'slug': 'translations',
            'ajax': '{!! route('admin.translations.data') !!}'
        }
    </script>
@endsection

@section('container')

    <div class="modal fade #modal-primary" data-keyboard="false" data-backdrop="static" id="addEditModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="add-label">Add New Translation</h4>
                    <h4 class="modal-title" style="display: none" id="edit-label">Edit Translation</h4>
                </div>
                <div class="modal-body">
                    <form id="item-form">
                        <input type="hidden" id="id" name="id" value="0">
                        <div class="form-group">
                            <select name="locale" id="locale" class="form-control">
                                <option value="ar">عربي</option>
                                <option value="en">English</option>
                            </select>
                            {{--<input type="text" class="form-control" data-validation="required" id="locale" name="locale" placeholder="Language">--}}
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" data-validation="required" id="group" name="group" placeholder="Group">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" data-validation="required" id="item" name="item" placeholder="item">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" data-validation="required" id="text" name="text" placeholder="Translation"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info submit-form" id="save-item">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Translations</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> {{ __('admin.home') }}</a></li>
            <li class="active">Translations</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->


        <div class="row">
            <div class="col-xs-12">

                <div class="box #box-danger #box-solid">

                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <!-- tools box -->
                        @if(can('add-translation'))
                        <div class="pull-right box-tools">
                            {{--<a href="#" onclick="return false;" id="addNewItem" class="btn btn-success pull-left">Add new translation</a>--}}
                        </div>
                        @endif
                        <!-- /. tools -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">

                        <table id="datatable" class="table table-bordered table-striped" data-total="{{ $recordsTotal }}">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Group</th>
                                <th>Translation</th>
                                <th>Language</th>
                                <th>{{ __('admin.options') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($translations as $translation)
                            <tr>
                                <td>{{ $translation->id }}</td>
                                <td width="30%">{!! $translation->item !!}</td>
                                <td width="30%">{!! $translation->group !!}</td>
                                <td width="30%">{!! $translation->text !!}</td>
                                <td width="30%">{!! $translation->locale !!}</td>
                                <td>{!! $translation->options !!}</td>
                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Group</th>
                                <th>Translation</th>
                                <th>Language</th>
                                <th>{{ __('admin.options') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>


    </section><!-- /.content -->

@endsection
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/backend/vendor/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">The Modules</li>
            <!-- Optionally, you can add icons to the links -->
            <li @if($current == 'home') class="active"@endif()><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            @if(can(['add-user', 'edit-user', 'delete-user']))
                <li @if($current == 'users') class="active"@endif()><a href="{{ route('admin.users') }}"><i class="fa fa-users"></i> <span>Users</span></a></li>
            @endif

            @if(can(['add-city', 'edit-city', 'delete-city', 'add-neighborhood', 'edit-neighborhood', 'delete-neighborhood']))
            <li class="treeview @if($current == 'cities' or $current == 'neighborhoods')active @endif()">
                <a href="#"><i class="fa fa-eercast"></i> <span>Cities and neighborhoods</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(isset($current) && $current == 'cities') class="active" @endif()><a href="{{ route('admin.cities') }}"><i class="fa fa-list"></i> Cities</a></li>
                    <li @if(isset($current) && $current == 'neighborhoods') class="active" @endif()><a href="{{ route('admin.neighborhoods') }}"><i class="fa fa-list-alt"></i> Neighborhoods</a></li>
                </ul>
            </li>
            @endif()

            @if(can(['add-mosque', 'edit-mosque', 'delete-mosque']))
            <li @if($current == 'mosques') class="active"@endif()><a href="{{ route('admin.mosques') }}"><i class="fa fa-eercast"></i> <span>Mosques</span></a></li>
            @endif
            @if(can([
                'add-company', 'edit-company', 'delete-company',
                'add-product', 'edit-product', 'delete-product',
            ]))
                <li class="treeview @if($current == 'companies' or $current == 'products')active @endif()">
                    <a href="#"><i class="fa fa-eercast"></i> <span>Water Companies</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li @if(isset($current) && $current == 'companies') class="active" @endif()><a href="{{ route('admin.companies') }}"><i class="fa fa-list"></i> List Companies</a></li>
                        <li @if(isset($current) && $current == 'products') class="active" @endif()><a href="{{ route('admin.products') }}"><i class="fa fa-list"></i> Products</a></li>
                    </ul>
                </li>
            @endif
            @if(auth()->user()->isCompany())
                <li class="treeview active">
                    <a href="#"><i class="fa fa-eercast"></i> <span>My Company</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li @if(isset($current) && $current == 'companies') class="active" @endif()><a href="{{ route('admin.companies') }}"><i class="fa fa-list"></i> Company Details</a></li>
                        <li @if(isset($current) && $current == 'companies') class="active" @endif()><a href="{{ route('admin.companies') }}"><i class="fa fa-list"></i> Our products</a></li>
                        <li @if(isset($current) && $current == 'companies') class="active" @endif()><a href="{{ route('admin.companies') }}"><i class="fa fa-list"></i> Covered Places</a></li>
                    </ul>
                </li>
            @endif
            @if(can(['add-translation', 'edit-translation', 'delete-translation']))
                <li @if($current == 'translations') class="active"@endif()><a href="{{ route('admin.translations') }}"><i class="fa fa-language"></i> <span>Translations</span></a></li>
            @endif
            @if(can(['view-order', 'delete-order']))
                <li @if($current == 'orders') class="active"@endif()><a href="{{ route('admin.orders') }}"><i class="fa fa-user-o"></i> <span>Orders</span></a></li>
            @endif
            @if(can(['view-contacts', 'edit-contact', 'delete-contact']))
                <li @if($current == 'contacts') class="active"@endif()><a href="{{ route('admincontacts.index') }}"><i class="fa fa-user-o"></i> <span>Contact us</span></a></li>
            @endif
            @if(can(['edit-options']))
                <li @if($current == 'options') class="active"@endif()><a href="{{ route('admin.options.index') }}"><i class="fa fa-cogs"></i> <span>Options</span></a></li>
            @endif
            <li><a href="{{ route('admin.logout') }}"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
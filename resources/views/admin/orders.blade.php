@extends('admin.layouts.app')

@section('before_scripts')
    <script>
        var pageInfo = {
            'slug': 'orders',
            'ajax': '{!! route('admin.orders.data') !!}'
        }
    </script>
@endsection
@section('after_scripts')
    <!-- DataTables -->
    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('container')

    <div class="modal fade #modal-primary" data-keyboard="false" data-backdrop="static" id="addEditModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="add-label">Add New Order</h4>
                    <h4 class="modal-title" style="display: none" id="edit-label">Edit Order</h4>
                </div>
                <div class="modal-body">
                    <form id="item-form">
                        <input type="hidden" id="id" name="id" value="0">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" data-validation="required" id="name" name="name" placeholder="Order name">
                                </div>
                                <div class="form-group">
                                    <label for="defined">Order order</label>
                                    <input type="number" class="form-control" data-validation="required" id="order_order" name="order_order" placeholder="Order order">
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info submit-form" id="save-item">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Orders</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Orders</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->


        <div class="row">
            <div class="col-xs-12">

                <div class="box">


                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <!-- tools box -->
                        @if(can('add-order'))
                        <div class="pull-right box-tools">
                            <a href="#" onclick="return false;" id="addNewItem" class="btn btn-success pull-left">Add new order</a>
                        </div>
                        @endif
                        <!-- /. tools -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatable" class="table table-bordered table-striped" data-total="{{ $recordsTotal }}">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Products</th>
                                <th>Price</th>
                                <th>Mosque</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td width="20%">{!! $order->name !!}</td>
                                <td width="20%">{!! $order->products !!}</td>
                                <td width="20%">{!! $order->price !!}</td>
                                <td width="20%">{!! $order->mosque !!}</td>
                                <td>{!! $order->options !!}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>


    </section><!-- /.content -->

@endsection
@extends('admin.layouts.app')

@section('before_scripts')
    <script>
        var pageInfo = {
            'slug': 'companies',
            'ajax': '{!! route('admin.companies.data') !!}'
        }
    </script>
@endsection
@section('after_scripts')
    <!-- DataTables -->
    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('container')

    <div class="modal fade #modal-primary" data-keyboard="false" data-backdrop="static" id="addEditModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="add-label">Add New Company</h4>
                    <h4 class="modal-title" style="display: none" id="edit-label">Edit Company</h4>
                </div>
                <div class="modal-body">
                    <form id="item-form">
                        <input type="hidden" id="id" name="id" value="0">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" data-validation="required" id="name" name="name" placeholder="Company name">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" data-validation="required" id="description" name="description" placeholder="Company description"></textarea>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<label for="city_id">City</label>--}}
                                    {{--<select class="form-control" name="city_id" id="city_id">--}}
                                        {{--<option value="0" disabled selected>Select city</option>--}}
                                    {{--@foreach(\App\City::all() as $city)--}}
                                        {{--<option value="{{ $city->id }}">{{ $city->name }}</option>--}}
                                    {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="neighborhood_id">Neighborhood</label>--}}
                                    {{--<select class="form-control" name="neighborhood_id" id="neighborhood_id"></select>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label for="user_id">User</label>
                                    <select class="form-control" name="user_id" id="user_id">
                                        <option value="0" disabled selected>Select user</option>
                                        @foreach(\App\User::all() as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="file">Image</label>
                                    <input class="form-control" type="file" id="file" name="file">
                                </div>
                                <div class="form-group">
                                    <label>Covered Places</label>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>City</th>
                                            <th>Neighborhood</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody id="places">
                                        <tr>
                                            <td>City</td>
                                            <td>Neighborhood</td>
                                            <td>Delete</td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td>
                                                <select class="form-control" name="city_id" id="city_id">
                                                    <option value="0" disabled selected>Select city</option>
                                                    @foreach(\App\City::all() as $city)
                                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control" name="neighborhood_id" id="neighborhood_id"></select>
                                            </td>
                                            <td>
                                                <a href="#" class="btn btn-primary btn-sm" id="add_place">ADD</a>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info submit-form" id="save-item">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Companies</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Companies</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->


        <div class="row">
            <div class="col-xs-12">

                <div class="box">


                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <!-- tools box -->
                        @if(can('add-company'))
                        <div class="pull-right box-tools">
                            <a href="#" onclick="return false;" id="addNewItem" class="btn btn-success pull-left">Add new company</a>
                        </div>
                        @endif
                        <!-- /. tools -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatable" class="table table-bordered table-striped" data-total="{{ $recordsTotal }}">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Places</th>
                                <th>User</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($companies as $company)
                            <tr>
                                <td>{{ $company->id }}</td>
                                <td width="20%">{!! $company->image !!}</td>
                                <td width="30%">{!! $company->name !!}</td>
                                <td width="20%">{!! $company->places !!}</td>
                                <td width="20%">{!! $company->user !!}</td>
                                <td>{!! $company->options !!}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>


    </section><!-- /.content -->

@endsection
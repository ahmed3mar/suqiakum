@extends('admin.layouts.app')

@section('before_scripts')
    <script>
        var pageInfo = {
            'slug': 'mosques',
            'ajax': '{!! route('admin.mosques.data') !!}'
        }
    </script>
@endsection
@section('after_scripts')
    <!-- DataTables -->
    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset ("/backend/vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('container')

    <div class="modal fade #modal-primary" data-keyboard="false" data-backdrop="static" id="addEditModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="add-label">Add New Mosque</h4>
                    <h4 class="modal-title" style="display: none" id="edit-label">Edit Mosque</h4>
                </div>
                <div class="modal-body">
                    <form id="item-form">
                        <input type="hidden" id="id" name="id" value="0">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" data-validation="required" id="name" name="name" placeholder="Mosque name">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" data-validation="required" id="description" name="description" placeholder="Company description"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="city_id">City</label>
                                    <select class="form-control" name="city_id" id="city_id">
                                        <option value="0" disabled selected>Select city</option>
                                    @foreach(\App\City::all() as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="file">Image</label>
                                    <input class="form-control" type="file" id="file" name="file">
                                </div>
                                <div class="form-group">
                                    <label for="neighborhood_id">Neighborhood</label>
                                    <select class="form-control" name="neighborhood_id" id="neighborhood_id"></select>
                                </div>
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input type="text" name="address" id="address" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="latitude">latitude</label>
                                    <input type="text" name="latitude" id="latitude" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="longitude">longitude</label>
                                    <input type="text" name="longitude" id="longitude" class="form-control">
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info submit-form" id="save-item">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Mosques</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Mosques</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->


        <div class="row">
            <div class="col-xs-12">

                <div class="box">


                    <div class="box-header">
                        <h3 class="box-title"></h3>
                        <!-- tools box -->
                        @if(can('add-mosque'))
                        <div class="pull-right box-tools">
                            <a href="#" onclick="return false;" id="addNewItem" class="btn btn-success pull-left">Add new mosque</a>
                        </div>
                        @endif
                        <!-- /. tools -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatable" class="table table-bordered table-striped" data-total="{{ $recordsTotal }}">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>City</th>
                                <th>Neighborhood</th>
                                <th>Address</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($mosques as $mosque)
                            <tr>
                                <td>{{ $mosque->id }}</td>
                                <td width="20%">{!! $mosque->image !!}</td>
                                <td width="30%">{!! $mosque->name !!}</td>
                                <td width="20%">{!! $mosque->city !!}</td>
                                <td width="20%">{!! $mosque->neighborhood !!}</td>
                                <td width="10%">{!! $mosque->address !!}</td>
                                <td>{!! $mosque->options !!}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>


    </section><!-- /.content -->

@endsection
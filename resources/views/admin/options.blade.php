@extends('admin.layouts.app')

@section('before_scripts')
    <script>
        var pageInfo = {
            'slug': 'options',
            'ajax': '{!! route('admin.options.update') !!}'
        }
    </script>
@endsection

@section('container')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Options</h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.main') }}"><i class="fa fa-dashboard"></i> {{ __('admin.home') }}</a></li>
            <li class="active">Options</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->


        <div class="row">
            <div class="col-xs-12">

                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Global Options</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" id="options-global" class="form-horizontal">
                        <div class="box-body">
                            <input type="hidden" name="type" value="global">

                            <div class="form-group">
                                <label for="site_title" class="col-sm-2 control-label">Site title</label>

                                <div class="col-sm-5">
                                    <input data-validation="required" type="text" class="form-control" name="site_title" id="site_title" placeholder="Site title (english)" value="{{ option('site_title') }}">
                                </div>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="site_title_ar" id="site_title_ar" placeholder="Site title (arabic)" value="{{ option('site_title_ar') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="home_title" class="col-sm-2 control-label">Home title</label>

                                <div class="col-sm-5">
                                    <input data-validation="required" type="text" class="form-control" name="home_title" id="home_title" placeholder="Home title (english)" value="{{ option('home_title') }}">
                                </div>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="home_title_ar" id="home_title_ar" placeholder="Home title (arabic)" value="{{ option('home_title_ar') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="site_description" class="col-sm-2 control-label">Site description</label>

                                <div class="col-sm-5">
                                    <textarea data-validation="required" class="form-control" name="site_description" id="site_description" placeholder="Site description (english)">{{ option('site_description') }}</textarea>
                                </div>

                                <div class="col-sm-5">
                                    <textarea class="form-control" name="site_description_ar" id="site_description_ar" placeholder="Site description (arabic)">{{ option('site_description_ar') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="site_keywords" class="col-sm-2 control-label">Site keywords</label>

                                <div class="col-sm-5">
                                    <input data-validation="required" type="text" class="form-control" name="site_keywords" id="site_keywords" placeholder="Site keywords (english)" value="{{ option('site_keywords') }}">
                                    <p class="help-block keep">Seprate between words by comma ","</p>
                                </div>

                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="site_keywords_ar" id="site_keywords_ar" placeholder="Site keywords (arabic)" value="{{ option('site_keywords_ar') }}">
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            {{--<button type="submit" class="btn btn-default">Cancel</button>--}}
                            <button id="submit-global" disabled="disabled" class="btn btn-info pull-right">Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->


                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Site Close</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" id="options-close" class="form-horizontal">
                        <input type="hidden" name="type" value="close">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="site_title" class="col-sm-2 control-label">Site close?</label>

                                <div class="col-sm-10">
                                    <label class="radio-inline"><input type="radio" name="site_close" value="1" @if( option('site_close') ) checked @endif()>Yes</label>
                                    <label class="radio-inline"><input type="radio" name="site_close" value="0" @if( !option('site_close') ) checked @endif()>No</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="site_description" class="col-sm-2 control-label">Close Message</label>

                                <div class="col-sm-5">
                                    <textarea data-validation="required" class="form-control" name="site_close_message" id="site_close_message" placeholder="Site close message (english)">{{ option('site_close_message') }}</textarea>
                                </div>

                                <div class="col-sm-5">
                                    <textarea class="form-control" name="site_close_message_ar" id="site_close_message_ar" placeholder="Site close message (arabic)">{{ option('site_close_message_ar') }}</textarea>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            {{--<button type="submit" class="btn btn-default">Cancel</button>--}}
                            <button id="submit-close" disabled="disabled" class="btn btn-info pull-right">Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>


    </section><!-- /.content -->

@endsection
@extends('layouts.app')
@section('content')

    <section class="page-header text-left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb text-left">
                        <li><a href="#">الرئيسية</a></li>
                        <li class="active">عرض المساجد</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-left">
                    <h1 class="text-left">عرض المساجد</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <hr class="tall">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h1 class="mb-none"><strong>المساجد</strong></h1>
            </div>
        </div>

        <div class="row">

            <div class="masonry-loader masonry-loader-showing">
                <ul class="products product-thumb-info-list" data-plugin-masonry>
                    @foreach($mosques as $mosque)
                    <li class="col-md-3 col-sm-6 col-xs-12 product">
									<span class="product-thumb-info">
										<a href="{{ $mosque->url }}">
											<span class="">
										<img src="{{ $mosque->image_url }}" class="img-responsive" alt="">
											</span>
										</a>
										<span class="product-thumb-info-content">
											<a href="{{ $mosque->url }}" class="text-left">
											<span class="thumb-info-caption-title text-uppercase text-color-dark font-weight-bold">{{ $mosque->name }}</span>
											<p>{{ $mosque->city->name }} - {{ $mosque->neighborhood->name }}</p>
											</a>
										</span>
									</span>
                    </li>
                    @endforeach
                </ul>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">

                @if($mosques)
                {{ $mosques->links() }}
                @endif
            </div>
        </div>

    </div>

@endsection

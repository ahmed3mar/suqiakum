@extends('layouts.app')

@section('after_scripts')
    <script>
        $('#company_id').on('change', function () {
            $.ajax({
                url: '/en/order/company/' + $(this).val(),
                type: 'post',
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                success: function (response) {
                    if(response.status == 'ok') {

                        var products = response.result.products;
                        $('#product_id').html('');
                        for(var i in products) {
                            $('#product_id').append('<option data-price="'+products[i].price+'" value="'+products[i].id+'">'+ products[i].name +'</option>')
                        }
                        $('#product_id').trigger('change');

                        var mosques = response.result.mosques;
                        $('#mosque_id').html('');
                        for(var i in products) {
                            $('#mosque_id').append('<option value="'+mosques[i].id+'">'+ mosques[i].name +'</option>')
                        }
                        $('#mosque_id').trigger('change');

                    }
                },
                dataType: 'json',
            })
        });
        $('#quantity,#product_id').on('change', function () {
            @if(isset($product))
            $('#price').val({{ $product->price }});
            $('#total').val({{ $product->price }} * $('#quantity').val());
            @else
            $('#price').val(parseInt($('#product_id option:selected').data('price')));
            $('#total').val(parseInt($('#product_id option:selected').data('price')) * $('#quantity').val());
            @endif
        });

        $(document).on('click', '.remove-product', function () {
            $(this).parent().parent().remove();
            return false;
        })

        $(document).on('click', '.minus', function () {
            if(parseInt($(this).parent().find('.qty').val()) == 1) return false;
            $(this).parent().find('.qty').val(parseInt($(this).parent().find('.qty').val()) - 1);

            var price = $(this).parent().parent().parent().find('.product-price .amount').text().replace('$', '')
            price = parseInt(price)
            $(this).parent().parent().parent().find('.product-subtotal .amount').text('$' + (price * $(this).parent().find('.qty').val()))

            return false;
        })

        $(document).on('click', '.plus', function () {
            $(this).parent().find('.qty').val(parseInt($(this).parent().find('.qty').val()) + 1);

            var price = $(this).parent().parent().parent().find('.product-price .amount').text().replace('$', '')
            price = parseInt(price)
            $(this).parent().parent().parent().find('.product-subtotal .amount').text('$' + (price * $(this).parent().find('.qty').val()))

            return false;
        })

        $('#add-product-cart').click(function () {
            if(!$('#product_id').val()) return false;

            var $i = $('#products-list tr').length;

            var $product_html = '<tr class="cart_table_item">\n' +
                '                                                    <td class="product-remove">\n' +
                '                                                        <a title="حذف هذا المنتج" class="remove remove-product" href="#">\n' +
                '                                                            <i class="fa fa-times"></i>\n' +
                '                                                        </a>\n' +
                '                                                    </td>\n' +
                '                                                    <td class="product-thumbnail">\n' +
                '                                                        <a href="#">\n' +
                '                                                            <img width="100" height="100" alt="" class="img-responsive" src="img/products/product-1.jpg">\n' +
                '                                                        </a>\n' +
                '                                                    </td>\n' +
                '                                                    <td class="product-name">\n' +
                '                                                        <a href="#">'+ $('#product_id option:selected').text() +'</a>\n' +
                '                                                    </td>\n' +
                '                                                    <td class="product-price">\n' +
                '                                                        <span class="amount">$'+parseInt($('#product_id option:selected').data('price'))+'</span>\n' +
                '                                                    </td>\n' +
                '                                                    <td class="product-quantity">\n' +
                '                                                            <div class="quantity">\n' +
                '                                                                <input type="button" class="minus" value="-">\n' +
                '                                                                <input type="hidden"  value="'+$('#product_id').val()+'" name="products['+$i+'][product_id]">\n' +
                '                                                                <input type="hidden"  value="'+parseInt($('#product_id option:selected').data('price'))+'" name="products['+$i +'][price]">\n' +
                '                                                                <input type="text" class="input-text qty text" title="Qty" value="1" name="products['+$i+'][quantity]" min="1" step="1">\n' +
                '                                                                <input type="button" class="plus" value="+">\n' +
                '                                                            </div>\n' +
                '                                                    </td>\n' +
                '                                                    <td class="product-subtotal">\n' +
                '                                                        <span class="amount">$'+parseInt($('#product_id option:selected').data('price'))+'</span>\n' +
                '                                                    </td>\n' +
                '                                                </tr>';
            $('#products-list').append($product_html);
            return false;
        })

        $('#order-submit').submit(function () {

            $('.has-error').removeClass('has-error');

            $.ajax({
                url: '/en/order',
                type: 'post',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'ok') {
                        $('#order-submit').slideUp('normal', function () {
                            $('#order-done').slideDown();
                        })
                    }
                },
                error: function(xhr) {
                    var json = JSON.parse(xhr.responseText);
                    for(var i in json.errors) {
                        $('#' + i).parent().parent().addClass('has-error');
                    }
                },
                dataType: 'json',
            });

            return false;
        })

    </script>
@endsection

@section('content')

    <section class="page-header text-left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb text-left">
                        <li><a href="#">الرئيسية</a></li>
                        <li class="active">نموذج شراء مياة</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-left">
                    <h1 class="text-left">نموذج شراء مياة</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row">

            <div class="col-xs-12">
                <div class="featured-box featured-box-primary align-left mt-sm">
                    <div class="box-content">



                        <section id="order-done" class="call-to-action call-to-action-tertiary mb-xl" style="display: none">
                            <div class="call-to-action-content" style="width: 100%">
                                <h3>تم ارسال طلبك بنجاح</h3>
                                <p>سيتم التواصل معك في اسرع وقت ممكن.</p>
                            </div>
                        </section>

                        <form id="order-submit" class="form-horizontal form-bordered" method="post" action="{{ route('order.submit') }}">


                            <section class="panel panel-admin">

                                <div class="panel-body">


                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">شركة المياة</label>
                                            <div class="col-md-9">
                                                @if(isset($company))
                                                    <input class="form-control transparent-disabled" type="text" readonly value="{{ $company->name }}">
                                                    <input type="hidden" name="company_id" id="company_id" value="{{ $company->id }}">
                                                @else
                                                    <select id="company_id" name="company_id" data-plugin-selectTwo class="form-control populate" required>
                                                        <option value="0" disabled selected>شركة المياة</option>
                                                        @foreach($companies as $co)
                                                            <option value="{{ $co->id }}"@if(old('company_id')) selected @endif()>{{ $co->name }}</option>
                                                        @endforeach
                                                    </select>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">المنتج</label>
                                            <div class="col-md-7">
                                                @if(isset($product))
                                                    <input class="form-control transparent-disabled" type="text" readonly value="{{ $product->name }}">
                                                    <input type="hidden" name="product_id" id="product_id" value="{{ $product->id }}">
                                                @else
                                                    <select data-plugin-selectTwo class="form-control populate" name="product_id" id="product_id" required>
                                                        @if(isset($company))
                                                            <option data-price="0" value="0" disabled selected>اختر المنتج</option>
                                                            @foreach($company->products as $pro)
                                                                <option data-price="{{ $pro->price }}" value="{{ $pro->id }}" @if(old('product_id')) selected @endif()>{{ $pro->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                @endif
                                            </div>
                                            <div class="col-md-2">
                                                @if(!isset($product))
                                                <a href="#" class="btn btn-primary" id="add-product-cart">اضافة</a>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <table class="shop_table cart">
                                                <thead>
                                                <tr>
                                                    <th class="product-remove">
                                                        &nbsp;
                                                    </th>
                                                    <th class="product-thumbnail">
                                                        &nbsp;
                                                    </th>
                                                    <th class="product-name">
                                                        المنتج
                                                    </th>
                                                    <th class="product-price">
                                                        السعر
                                                    </th>
                                                    <th class="product-quantity">
                                                        الكمية
                                                    </th>
                                                    <th class="product-subtotal">
                                                        المجموع
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody id="products-list">

                                                @if(isset($product))
                                                    <tr class="cart_table_item">
                                                        <td class="product-remove">
                                                            {{--<a title="حذف هذا المنتج" class="remove remove-product" href="#">--}}
                                                                {{--<i class="fa fa-times"></i>--}}
                                                            {{--</a>--}}
                                                        </td>
                                                        <td class="product-thumbnail">
                                                            {{--<a href="#">--}}
                                                                {{--<img width="100" height="100" alt="" class="img-responsive" src="img/products/product-1.jpg">--}}
                                                            {{--</a>--}}
                                                        </td>
                                                        <td class="product-name">
                                                            <a href="#">{{ $product->name }}</a>
                                                        </td>
                                                        <td class="product-price">
                                                            <span class="amount">{{ $product->price }} ريال</span>
                                                        </td>
                                                        <td class="product-quantity">
                                                            <div class="quantity">
                                                                <input type="button" class="minus" value="-">
                                                                <input type="hidden" value="{{ $product->id }}" name="products[0][product_id]">
                                                                <input type="hidden" value="{{ $product->price }}" name="products[0][price]">
                                                                <input type="text" class="input-text qty text" title="Qty" value="1" name="products[0][quantity]" min="1" step="1">
                                                                <input type="button" class="plus" value="+">
                                                            </div>
                                                        </td>
                                                        <td class="product-subtotal">
                                                            <span class="amount">{{ $product->price }} ريال</span>
                                                        </td>
                                                    </tr>
                                                @endif
                                                {{--<tr>--}}
                                                    {{--<td class="actions" colspan="6">--}}
                                                        {{--<div class="actions-continue">--}}
                                                        {{--</div>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                                </tbody>
                                            </table>

                                            <hr>
                                        </div>

                                        {{--<div class="form-group">--}}
                                            {{--<label class="col-md-3 control-label">المنتج</label>--}}
                                            {{--<div class="col-md-4">--}}
                                                {{--@if(isset($product))--}}
                                                    {{--<input class="form-control transparent-disabled" type="text" readonly value="{{ $product->name }}">--}}
                                                    {{--<input type="hidden" name="product_id" id="product_id" value="{{ $product->id }}">--}}
                                                {{--@else--}}
                                                    {{--<select data-plugin-selectTwo class="form-control populate" name="product_id" id="product_id" required>--}}
                                                        {{--@if(isset($company))--}}
                                                            {{--<option data-price="0" value="0" disabled selected>اختر المنتج</option>--}}
                                                            {{--@foreach($company->products as $pro)--}}
                                                                {{--<option data-price="{{ $pro->price }}" value="{{ $pro->id }}" @if(old('product_id')) selected @endif()>{{ $pro->name }}</option>--}}
                                                            {{--@endforeach--}}
                                                        {{--@endif--}}
                                                    {{--</select>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                            {{--<label class="col-md-2 control-label" for="quantity">الكمية</label>--}}
                                            {{--<div class="col-md-3">--}}
                                                {{--<input type="number" class="form-control" value="1" id="quantity" name="quantity" required>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        {{--<div class="form-group">--}}
                                            {{--<label class="col-md-3 control-label">سعر المنتج</label>--}}
                                            {{--<div class="col-md-4">--}}
                                                {{--<input id="price" type="text" class="form-control transparent-disabled" readonly value="@if(isset($product)){{ $product->price }}@else() 0 @endif()">--}}
                                            {{--</div>--}}
                                            {{--<label class="col-md-2 control-label">الاجمالي</label>--}}
                                            {{--<div class="col-md-3">--}}
                                                {{--<input id="total" type="text" class="form-control transparent-disabled" readonly value="@if(isset($product)){{ $product->price }}@else()0 @endif()">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">المسجد</label>
                                            <div class="col-md-9">
                                                @if(isset($mosque))
                                                    <input class="form-control transparent-disabled" type="text" readonly value="{{ $mosque->name }}">
                                                    <input type="hidden" name="mosque_id" id="mosque_id" value="{{ $mosque->id }}">
                                                @else
                                                    <select data-plugin-selectTwo class="form-control populate" name="mosque_id" id="mosque_id" required>
                                                        @if(isset($mosques))
                                                            <option disabled selected>اختر المسجد</option>
                                                            @foreach($mosques as $mosque)
                                                                <option value="{{ $mosque->id }}">{{ $mosque->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                @endif
                                            </div>
                                        </div>

                                        <hr />

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">رقم الهاتف</label>
                                            <div class="col-md-9">
                                                <input type="text" id="phone" name="phone" class="form-control" value="{{ old('phone') }}" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">الاسم</label>
                                            <div class="col-md-9">
                                                <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">العنوان</label>
                                            <div class="col-md-9">
                                                <input type="text" id="address" name="address" class="form-control" value="{{ old('address') }}" required>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-9 col-md-push-3">
                                                <input type="submit" value="ارسال الطلب" class="btn btn-primary btn-block mb-xl" data-loading-text="Loading...">
                                            </div>
                                        </div>


                                </div>
                            </section>


                        </form>
                    </div>
                </div>
            </div>
            {{--<div class="col-xs-4">--}}

                {{--<h4 class="heading-primary">Cart Totals</h4>--}}
                {{--<table class="cart-totals">--}}
                    {{--<tbody>--}}
                    {{--<tr class="cart-subtotal">--}}
                        {{--<th>--}}
                            {{--<strong>Cart Subtotal</strong>--}}
                        {{--</th>--}}
                        {{--<td>--}}
                            {{--<strong><span class="amount">$431</span></strong>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    {{--<tr class="shipping">--}}
                        {{--<th>--}}
                            {{--Shipping--}}
                        {{--</th>--}}
                        {{--<td>--}}
                            {{--Free Shipping<input type="hidden" value="free_shipping" id="shipping_method" name="shipping_method">--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    {{--<tr class="total">--}}
                        {{--<th>--}}
                            {{--<strong>Order Total</strong>--}}
                        {{--</th>--}}
                        {{--<td>--}}
                            {{--<strong><span class="amount">$431</span></strong>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    {{--</tbody>--}}
                {{--</table>--}}

            {{--</div>--}}

        </div>

    </div>



@endsection

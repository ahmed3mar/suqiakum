@extends('layouts.app')
@section('content')

    <section class="page-header text-left">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb text-left">
                        <li><a href="#">الرئيسية</a></li>
                        <li class="active">عرض شركات المياه</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-left">
                    <h1 class="text-left">عرض شركات المياه</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <hr class="tall">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h1 class="mb-none"><strong>شركات المياه</strong></h1>
            </div>
        </div>

        <div class="row">

            <div class="masonry-loader masonry-loader-showing">
                <ul class="products product-thumb-info-list" data-plugin-masonry>
                    @foreach($companies as $company)
                        <li class="col-md-3 col-sm-6 col-xs-12 product">
									<span class="product-thumb-info">
										<a href="{{ $company->url }}">
											<span class="">
                                            <img src="{{ $company->image_url }}" class="img-responsive img-main" alt="">
											</span>
										</a>
										<span class="product-thumb-info-content">
											<a href="{{ $company->url }}" class="text-left">
											<span class="thumb-info-caption-title text-uppercase text-color-dark font-weight-bold">{{ $company->name }}</span>
											{{--<p>معلومات عن شركة المياه</p>--}}
											</a>
										</span>
									</span>
                        </li>
                        @endforeach

                </ul>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                @if($companies)
                    {{ $companies->links() }}
                @endif
            </div>
        </div>

    </div>


@endsection

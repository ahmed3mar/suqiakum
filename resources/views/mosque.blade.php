@extends('layouts.app')
@section('content')

	<section class="page-header text-left">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb text-left">
						<li><a href="{{ route('home') }}">الرئيسية</a></li>
						<li class="active">عروض شركة المياه</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-left">
					<h1 class="text-left">عروض شركة المياه</h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<hr class="tall">
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">

				<div class="thumbnail">
					<img src="{{ $mosque->image_url }}" class="img-responsive img-rounded" alt="">
				</div>
			</div>

			<div class="col-md-6">

				<div class="summary entry-summary">

					<h1 class="mb-none"><strong>{{ $mosque->name }}</strong></h1>

					<hr class="tall" />
					<p class="taller">{{ $mosque->description }}</p>
					<hr class="tall" />
					<p class="taller address">{{ $mosque->address }}</p>
					<a href="{{ $mosque->order_url }}" class="btn btn-primary btn-icon">طلب شراء</a>

				</div>


			</div>
		</div>

	</div>


@endsection

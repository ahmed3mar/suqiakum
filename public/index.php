<?php

/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylor@laravel.com>
 */

define('LARAVEL_START', microtime(true));

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/
function trans($key = null, $replace = [], $locale = null)
{
    # sms.account_activation
    # sms.ad_activation

    if (is_null($key)) {
        return app('translator');
    }

    $exp = explode('.', $key, 2);
    $group = null;
    $item = $key;
    if( sizeof($exp) === 2 ) {
        $group = $exp[0];
        $item = $exp[1];
    }
    $if = \DB::table('translator_translations')->where('locale', app()->getLocale())->where('group', $group)->where('item', $item)->exists();
    if (!$if) {
        \DB::table('translator_translations')->insert([
            'locale'    => app()->getLocale(),
            'group'     => $group,
            'item'      => $item,
            'text'      => $item,
        ]);
    }

    return app('translator')->trans($key, $replace, $locale);
}
require __DIR__.'/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$response->send();

$kernel->terminate($request, $response);

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('image/{type}/{filename}', 'ImageController@index')
    ->name('image')
;

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localizationRedirect', 'localeViewPath' ],
], function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('company/{id}', 'CompaniesController@company')->name('company');
    Route::get('companies', 'CompaniesController@companies')->name('companies');
    Route::get('mosque/{id}', 'MosquesController@mosque')->name('mosque');
    Route::get('mosques', 'MosquesController@mosques')->name('mosques');
    Route::post('order/company/{id}', 'OrdersController@data')->name('order.data');
    Route::get('order/mosque/{id}', 'OrdersController@mosque')->name('order.mosque');
    Route::get('order/product/{id}', 'OrdersController@product')->name('order.product');
    Route::get('order/company/{id}', 'OrdersController@company')->name('order.company');
    Route::get('order', 'OrdersController@index')->name('order');
    Route::post('order', 'OrdersController@store')->name('order.submit');
});

# Admin Routes!
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin'], function () {

    Route::get('/', 'HomeController@index')->name('.main');

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('.login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('.logout');
    Route::get('logout', function () {
        Auth::logout();
        return redirect('/admin');
    })->name('.logout');

    Route::group(['middleware' => ['auth.admin']], function () {

        #if(function_exists('admin_resource')) {
            admin_resource('users', 'UsersController', 'users');
            admin_resource('translations', 'TranslationsController', 'translations');
            admin_resource('cities', 'CitiesController', 'cities');
            admin_resource('neighborhoods', 'NeighborhoodsController', 'neighborhoods');
            Route::get('neighborhoods/list/{city_id}', 'NeighborhoodsController@list')->name('.neighborhoods.list');
            admin_resource('mosques', 'MosquesController', 'mosques');
            admin_resource('companies', 'CompaniesController', 'companies');
            admin_resource('products', 'ProductsController', 'products');
            admin_resource('places', 'PlacesController', 'places');
            admin_resource('pages', 'PagesController', 'pages');
            admin_resource('orders', 'OrdersController', 'orders');
        #}

        Route::get('contacts/export', 'ContactsController@export')->name('.contacts.export');
        Route::post('contacts/data', 'ContactsController@data')->name('.contacts.data');
        Route::resource('contacts', 'ContactsController');

        Route::get('options', 'OptionsController@index')->name('.options.index');
        Route::post('options', 'OptionsController@update')->name('.options.update');

    });

});
